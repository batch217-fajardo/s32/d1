let http  = require("http");

let port = 4000;

let server = http.createServer((req, res) => {
	// http method: GET - retrieving/reading information

	if (req.url == "/items" && req.method == "GET" ){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		//Ends or the final output in the application/browser
		res.end("Data retrieved from the database");
	}

	// http method: POST - inserting data in the server/database

	if(req.url == "/items" && req.method == "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain" });
		res.end("Data to be sent to the database");
	}

});

server.listen(port);
console.log(`Server is running at local host: ${port}.`);