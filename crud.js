let http  = require("http");

// mock database - where we can save or insert data using variable

let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@mail.com"
	}

]

let port = 4000;

let server = http.createServer((req, res) => {
	// first endpoint "/users" - http method: GET
	if(req.url == "/users" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "application/json"});
		res.write(JSON.stringify(directory));
		res.end();
	}

	if(req.url == "/users" && req.method == "POST"){
		let requestBody = "";

		req.on("data", function(data){
			requestBody += data;
		});

		req.on("data", function(){
			console.log(typeof requestBody);

			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			}

			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {"Content-Type" : "application/json" })
			res.write(JSON.stringify(newUser));
			res.end();
		})

	}

});

server.listen(port);
console.log(`Server is running at local host: ${port}.`);